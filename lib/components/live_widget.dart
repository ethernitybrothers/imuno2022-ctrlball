import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../blocs/live.dart';
import '../config.dart';

class LiveWidget extends StatelessWidget {
  const LiveWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<LiveCubit, LiveState>(builder: createLiveColorWidget);
  }

  LiveColorWidget createLiveColorWidget(BuildContext context, LiveState state) {
    return LiveColorWidget(state.colors, Duration(milliseconds: state.colorTransitionMillis));
  }

}

class LiveColorWidget extends StatefulWidget {

  final List<Color> colors;
  final Duration duration;

  LiveColorWidget(this.colors, this.duration);

  @override
  State<LiveColorWidget> createState() {
    return LiveColorWidgetState();
  }
}

class LiveColorWidgetState extends State<LiveColorWidget> {
  final config = Config();

  LiveColorWidgetState();

  int colorIndex = 0;

  late Timer timer;

  @override
  void initState() {
    super.initState();
    colorIndex = 0;
    timer = Timer.periodic(widget.duration, (timer) => changeColor());
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  void changeColor() {
    setState(() {
      colorIndex = (colorIndex + 1) % widget.colors.length;
    });
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    timer.cancel();
    timer = Timer.periodic(widget.duration, (timer) => changeColor());
  }

  @override
  Widget build(BuildContext context) {
    var color = widget.colors[colorIndex % widget.colors.length];
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: AnimatedContainer(
            width: double.infinity,
            height: double.infinity,
            duration: widget.duration,
            decoration: BoxDecoration(color: color))
      );
  }
}
