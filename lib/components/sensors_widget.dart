import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imuno2022ctrlball/blocs/sensors.dart';
import 'package:imuno2022ctrlball/components/threevalues_widget.dart';

import '../config.dart';

const double GYROSCOPE_RANGE = 5;
const double MAGNETOMETER_RANGE = 50;
const double ACCELEROMETER_RANGE = 10;
const double USER_ACCELEROMETER_RANGE = 5;
const double ORIENTATION_RANGE = 3;

class HardSensorsWidget extends StatefulWidget {
  const HardSensorsWidget({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HardSensorsState();
}

class HardSensorsState extends State<HardSensorsWidget> {
  final config = Config();

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
          thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 7.0),
          overlayShape: const RoundSliderOverlayShape(overlayRadius: 16.0),
          trackHeight: 1, showValueIndicator: ShowValueIndicator.always),
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            GyroscopeWidget(
                onChanged: (value) => onChangeGyroscope(context, value)),
            MagnetometerWidget(
                onChanged: (value) => onChangeMagnetometer(context, value)),
            BatteryWidget(
                onChanged: (value) => onChangeBattery(context, value)),
            TemperatureWidget(
                onChanged: (value) => onChangeTemperature(context, value))
          ],
        ),
      ),
    );
  }

  void onChangeGyroscope(BuildContext context, bool value) {
    context
        .read<GyroscopeCubit>()
        .setEnable(value)
        .then((value) => setState(() {}));
  }

  void onChangeMagnetometer(BuildContext context, bool value) {
    context
        .read<MagnetometerCubit>()
        .setEnable(value)
        .then((value) => setState(() {}));
  }

  void onChangeBattery(BuildContext context, bool value) {
    context
        .read<BatteryCubit>()
        .setEnable(value)
        .then((value) => setState(() {}));
  }

  void onChangeTemperature(BuildContext context, bool value) {
    context
        .read<TemperatureCubit>()
        .setEnable(value)
        .then((value) => setState(() {}));
  }

}

class SoftSensorsWidget extends StatefulWidget {
  const SoftSensorsWidget({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => SoftSensorsState();
}

class SoftSensorsState extends State<SoftSensorsWidget> {
  final config = Config();

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
          thumbShape: const RoundSliderThumbShape(enabledThumbRadius: 7.0),
          overlayShape: const RoundSliderOverlayShape(overlayRadius: 16.0),
          trackHeight: 1, showValueIndicator: ShowValueIndicator.always),
      child: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            OrientationWidget(
                onChanged: (value) => onChangeOrientation(context, value)),
            AccelerometerWidget(
                onChanged: (value) => onChangeAccelerometer(context, value)),
            UserAccelerometerWidget(
                onChanged: (value) => onChangeUserAccelerometer(context, value)),
          ],
        ),
      ),
    );
  }

  void onChangeOrientation(BuildContext context, bool value) {
    context
        .read<OrientationCubit>()
        .setEnable(value)
        .then((value) => setState(() {}));
  }

  void onChangeUserAccelerometer(BuildContext context, bool value) {
    context
        .read<UserAccelerometerCubit>()
        .setEnable(value)
        .then((value) => setState(() {}));
  }

  void onChangeAccelerometer(BuildContext context, bool value) {
    context
        .read<AccelerometerCubit>()
        .setEnable(value)
        .then((value) => setState(() {}));
  }

}

class OrientationWidget extends StatelessWidget {
  OrientationWidget({this.onChanged, Key? key}) : super(key: key);

  final config = Config();

  final ValueChanged<bool>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FutureBuilder<bool>(
            future: config.getEnableOrientation(),
            builder: (context, snapshot) => SwitchListTile(
                title: const Text('ORIENTATION'),
                value: snapshot.hasData ? snapshot.data! : false,
                onChanged: onChanged)),
        BlocBuilder<OrientationCubit, OrientationState>(
            builder: (context, state) => ThreeValuesWidget(
                state.pitch, state.yaw, state.roll, ORIENTATION_RANGE)),
      ],
    );
  }
}

class GyroscopeWidget extends StatelessWidget {
  GyroscopeWidget({this.onChanged, Key? key}) : super(key: key);

  final config = Config();

  final ValueChanged<bool>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FutureBuilder<bool>(
          future: config.getEnableGyroscope(),
          builder: (context, snapshot) => SwitchListTile(
              title: const Text('GYROSCOPE'),
              value: snapshot.hasData ? snapshot.data! : false,
              onChanged: onChanged),
        ),
        BlocBuilder<GyroscopeCubit, GyroscopeState>(
            builder: (context, state) =>
                ThreeValuesWidget(state.x, state.y, state.z, GYROSCOPE_RANGE)),
      ],
    );
  }
}


class AccelerometerWidget extends StatelessWidget {
  AccelerometerWidget({this.onChanged, Key? key}) : super(key: key);

  final config = Config();

  final ValueChanged<bool>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FutureBuilder<bool>(
            future: config.getEnableAccelerometer(),
            initialData: false,
            builder: (context, snapshot) => SwitchListTile(
                title: const Text('ACCELEROMETER'),
                value: snapshot.hasData ? snapshot.data! : false,
                onChanged: onChanged)),
        BlocBuilder<AccelerometerCubit, AccelerometerState>(
            builder: (context, state) => ThreeValuesWidget(
                state.x, state.y, state.z, ACCELEROMETER_RANGE)),
      ],
    );
  }
}


class UserAccelerometerWidget extends StatelessWidget {
  UserAccelerometerWidget({this.onChanged, Key? key}) : super(key: key);

  final config = Config();

  final ValueChanged<bool>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        FutureBuilder<bool>(
            future: config.getEnableUserAccelerometer(),
            initialData: false,
            builder: (context, snapshot) => SwitchListTile(
                title: const Text('USER ACCELEROMETER'),
                value: snapshot.hasData ? snapshot.data! : false,
                onChanged: onChanged)),
        BlocBuilder<UserAccelerometerCubit, UserAccelerometerState>(
            builder: (context, state) => ThreeValuesWidget(
                state.x, state.y, state.z, USER_ACCELEROMETER_RANGE)),
      ],
    );
  }
}

class MagnetometerWidget extends StatelessWidget {
  MagnetometerWidget({this.onChanged, Key? key}) : super(key: key);

  final config = Config();

  final ValueChanged<bool>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      FutureBuilder<bool>(
          future: config.getEnableMagnetometer(),
          initialData: false,
          builder: (context, snapshot) => SwitchListTile(
              title: const Text('MAGNETOMETER'),
              value: snapshot.hasData ? snapshot.data! : false,
              onChanged: onChanged)),
      BlocBuilder<MagnetometerCubit, MagnetometerState>(
          builder: (context, state) =>
              ThreeValuesWidget(state.x, state.y, state.z, MAGNETOMETER_RANGE))
    ]);
  }
}

class BatteryWidget extends StatelessWidget {
  BatteryWidget({this.onChanged, Key? key}) : super(key: key);

  final config = Config();

  final ValueChanged<bool>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      FutureBuilder<bool>(
          future: config.getEnableBattery(),
          initialData: false,
          builder: (context, snapshot) => SwitchListTile(
              title: const Text('BATTERY'),
              value: snapshot.hasData ? snapshot.data! : false,
              onChanged: onChanged)),
      BlocBuilder<BatteryCubit, BatteryState>(
          builder: (context, state) => Text("${state.state}:${state.level}%")
         )
    ]);
  }
}

class TemperatureWidget extends StatelessWidget {
  TemperatureWidget({this.onChanged, Key? key}) : super(key: key);

  final config = Config();

  final ValueChanged<bool>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      FutureBuilder<bool>(
          future: config.getEnableTemperature(),
          initialData: false,
          builder: (context, snapshot) => SwitchListTile(
              title: const Text('TEMPERATURE'),
              value: snapshot.hasData ? snapshot.data! : false,
              onChanged: onChanged)),
      BlocBuilder<TemperatureCubit, TemperatureState>(
          builder: (context, state) => Text("${state.temperature} °C")
      )
    ]);
  }
}
