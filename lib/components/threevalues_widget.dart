import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const int DIVISIONS = 256;

class ThreeValuesWidget extends StatelessWidget {
  final double a;
  final double b;
  final double c;
  final double range;
  final int decimals;

  const ThreeValuesWidget(this.a, this.b, this.c, this.range,
      {this.decimals = 3, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Slider(
          value: a.clamp(-range, range),
          min: -range,
          max: range,
          divisions: DIVISIONS,
          onChanged: (double value) => {}),
      Slider(
          value: b.clamp(-range, range),
          min: -range,
          max: range,
          divisions: DIVISIONS,
          onChanged: (double value) => {}),
      Slider(
        value: c.clamp(-range, range),
        min: -range,
        max: range,
        divisions: DIVISIONS,
        onChanged: (double value) => {},
      ),
      Row(children: [
        Expanded(
            child: Align(
                alignment: Alignment.centerRight,
                child: Text(a.toStringAsFixed(decimals)))),
        Expanded(
            child: Align(
                alignment: Alignment.centerRight,
                child: Text(b.toStringAsFixed(decimals)))),
        Expanded(
            child: Align(
                alignment: Alignment.centerRight,
                child: Text(c.toStringAsFixed(decimals)))),
      ])
    ]);
  }
}
