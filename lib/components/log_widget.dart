import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imuno2022ctrlball/blocs/comm.dart';

class LogWidget extends StatelessWidget {
  const LogWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CommCubit, CommState>(builder: (context, state) {
      var list = state.logs.toList();
      return ListView.builder(
          // Let the ListView know how many items it needs to build.
          itemCount: list.length,
          // Provide a builder function. This is where the magic happens.
          // Convert each item into a widget based on the type of item it is.
          itemBuilder: (context, index) {
            final item = list[index];
            return ListTile(title: Text(item));
          });
    });
  }
}
