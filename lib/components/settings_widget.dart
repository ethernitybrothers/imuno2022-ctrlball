import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fullscreen/fullscreen.dart';
import 'package:imuno2022ctrlball/blocs/comm.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config.dart';

class SettingsWidget extends StatefulWidget {
  const SettingsWidget({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SettingsWidgetState();
}

class _SettingsWidgetState extends State<SettingsWidget> {
  final Config config = Config();
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late Future<String> _serverHostname;
  late Future<int> _serverPort;
  late Future<bool> _serverConnect;
  late Future<String> _ctrlBallName;

  TextEditingController? _controllerServerHostname;
  TextEditingController? _controllerServerPort;
  TextEditingController? _controllerCtrlBallName;

  bool? _serverConnectValue;

  Future<void> _onChange_ServerHostname(
      BuildContext context, String value) async {
    await config.setServerHostname(value);
    context.read<CommCubit>().setServerHostname(value);
  }

  Future<void> _onChange_ServerPort(BuildContext context, int value) async {
    await config.setServerPort(value);
    context.read<CommCubit>().setServerPort(value);
  }

  Future<void> _onChange_ServerConnect(BuildContext context, bool value) async {
    await config.setServerConnect(value);
    context.read<CommCubit>().setServerConnect(value);
    _serverConnectValue = value;
    setState(() {});
  }

  Future<void> _onChange_CtrlBallName(BuildContext context, String value) async {
    await config.setCtrlBallName(value);
    context.read<CommCubit>().setCtrlBallName(value);
  }

  @override
  void initState() {
    super.initState();
    _serverHostname = config.getServerHostname().then((String hostname) {
      _controllerServerHostname = TextEditingController(text: hostname);
      return hostname;
    });
    _serverPort = config.getServerPort().then((int port) {
      _controllerServerPort = TextEditingController(text: port.toString());
      return port;
    });
    _serverConnect = config.getServerConnect().then((value) {
      _serverConnectValue = value;
      setState(() {});
      return value;
    });
    _ctrlBallName = config.getCtrlBallName().then((String name) {
      _controllerCtrlBallName = TextEditingController(text: name);
      return name;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            FutureBuilder<String>(
              future: _serverHostname,
              builder: (context, snapshot) => TextField(
                  controller: _controllerServerHostname,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Server hostname',
                  ),
                  onChanged: (String value) =>
                      _onChange_ServerHostname(context, value)),
            ),
            const SizedBox(height: 15),
            FutureBuilder<int>(
              future: _serverPort,
              builder: (context, snapshot) => TextField(
                  controller: _controllerServerPort,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                  ],
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Server port',
                  ),
                  onChanged: (String value) =>
                      _onChange_ServerPort(context, int.parse(value))),
            ),
            const SizedBox(height: 15),
            FutureBuilder<String>(
                future: _ctrlBallName,
                builder: (context, snapshot) => TextField(
                    controller: _controllerCtrlBallName,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'CtrlBall name',
                    ),
                    onChanged: (String value) =>
                        _onChange_CtrlBallName(context, value))),
            const SizedBox(height: 15),
            SwitchListTile(
                title: const Text('Connect'),
                value: _serverConnectValue ?? false,
                onChanged: (bool value) =>
                    _onChange_ServerConnect(context, value)),
            const SizedBox(height: 15),
            BlocBuilder<CommCubit, CommState>(builder: (context, state) {
              return InkWell(
                onTap: () => onGoFullscreen(context),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: ColorFiltered(
                    colorFilter: ColorFilter.mode(
                        state.connected ? Colors.white : Colors.brown,
                        BlendMode.modulate,
                      ),
                    child: Image.asset("assets/images/ctrlball.png"),
                  ),
                ),
              );
            }),
          ],
        )
    );
  }

  void onGoFullscreen(BuildContext context) async {
    Navigator
        .pushNamed(context, '/live')
        .then((value) => FullScreen.enterFullScreen(FullScreenMode.LEANBACK));
  }
}
