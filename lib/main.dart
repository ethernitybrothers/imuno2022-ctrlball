import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imuno2022ctrlball/blocs/comm.dart';
import 'package:imuno2022ctrlball/blocs/sensors.dart';
import 'package:imuno2022ctrlball/components/live_widget.dart';
import 'package:wakelock/wakelock.dart';

import 'blocs/init.dart';
import 'blocs/live.dart';
import 'components/log_widget.dart';
import 'components/settings_widget.dart';
import 'components/sensors_widget.dart';

void main() {
  runApp(const CtrlBallApp());
}

class CtrlBallApp extends StatelessWidget {
  const CtrlBallApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    Wakelock.enable();
    return AppBlocs(
      child: MaterialApp(
          title: 'CtrlBall',
          theme: ThemeData(
            primarySwatch: Colors.brown,
          ),
          routes: <String, WidgetBuilder> {
            '/live': (BuildContext context) => LiveWidget(),
          },
          home: const CtrlBallPage()),
    );
  }
}


class AppBlocs extends StatelessWidget {
  final Widget child;

  AppBlocs({required this.child, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<InitCubit>(
      create: (context) => InitCubit(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => GyroscopeCubit(context)),
          BlocProvider(create: (context) => AccelerometerCubit(context)),
          BlocProvider(create: (context) => UserAccelerometerCubit(context)),
          BlocProvider(create: (context) => MagnetometerCubit(context)),
          BlocProvider(create: (context) => OrientationCubit(context)),
          BlocProvider(create: (context) => BatteryCubit(context)),
          BlocProvider(create: (context) => TemperatureCubit(context)),
          BlocProvider(create: (context) => PayloadCubit(context)),
        ],
        child: MultiBlocProvider(
          providers: [
            BlocProvider(create: (context) => CommCubit(context)),
            BlocProvider(create: (context) => LiveCubit(context), lazy: false),
          ],
          child: child,
        ),
      ),
    );
  }

}


class CtrlBallPage extends StatelessWidget {
  const CtrlBallPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: Text('IMUNO 2022 CtrlBall'),
          bottom: const TabBar(
            tabs: [
              Tab(icon: Icon(Icons.settings)),
              Tab(icon: Icon(Icons.sensors), text: "HW"),
              Tab(icon: Icon(Icons.sensors), text: "SOFT"),
              Tab(icon: Icon(Icons.report), text: "LOG")
            ],
          ),
        ),
        body: TabBarView(children: [
          SettingsWidget(),
          HardSensorsWidget(),
          SoftSensorsWidget(),
          LogWidget(),
        ]),
      ),
    );
  }
}
