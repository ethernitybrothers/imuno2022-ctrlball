
import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_vibrate/flutter_vibrate.dart';
import 'package:torch_light/torch_light.dart';

import 'comm.dart';


class LiveState {
  LiveState({required this.colors, required this.colorTransitionMillis});
  final List<Color> colors;
  final int colorTransitionMillis;
}

class LiveCubit extends Cubit<LiveState> {

  BuildContext context;
  late StreamSubscription payloadSubscription;

  static final initial = LiveState(colors: [Colors.white], colorTransitionMillis: 1000);

  LiveCubit(this.context) : super(initial) {
    final payloadCubit = context.read<PayloadCubit>();
    payloadSubscription = payloadCubit.stream.listen(onPayload);
  }

  @override
  Future<void> close() async {
    await payloadSubscription.cancel();
    return super.close();
  }

  Future<void> onPayload(Payload payload) async {
    var type = payload.payload['type'];
    if (type == "orb") {
      onOrb(payload.payload);
    }
  }

  void onOrb(Map payload) {
    if (payload.containsKey('colors')) {
      var colors = payload['colors'];
      var colorTransitionMillis = payload['colorTransitionMillis'] ?? state.colorTransitionMillis;
      var flutterColors = <Color>[];
      for (var color in colors) {
        var flutterColor = Color.fromRGBO(
            (color[0] * 255.0).floor(),
            (color[1] * 255.0).floor(),
            (color[2] * 255.0).floor(), 1.0);
        flutterColors.add(flutterColor);
      }
      emit(LiveState(colors: flutterColors, colorTransitionMillis: colorTransitionMillis));
    }
    if (payload.containsKey('torch')) {
      var torch = payload['torch'];
      if (torch) {
        TorchLight.enableTorch();
      }
      else {
        TorchLight.disableTorch();
      }
    }
    if (payload.containsKey('hapticFeedback')) {
      var hapticFeedback = payload['hapticFeedback'];
      var type = FeedbackType.values.firstWhere((e) => e.toString() == "FeedbackType.$hapticFeedback");
      Vibrate.feedback(type);
    }
  }

}
