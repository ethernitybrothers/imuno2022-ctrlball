import 'dart:async';
import 'package:battery_plus/battery_plus.dart';
import 'package:bloc/bloc.dart';
import 'package:devicetemperature/devicetemperature.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:imuno2022ctrlball/config.dart';
import 'package:motion_sensors/motion_sensors.dart';

import 'init.dart';

const UPDATE_INTERVAL = 100;

abstract class InstantEvent {
  InstantEvent() : time = now;
  InstantEvent.withTime(this.time);
  final int time;

  static int get now => DateTime.now().microsecondsSinceEpoch;

  Map toMap();
}

abstract class Vector3Event extends InstantEvent {
  Vector3Event(this.x, this.y, this.z);
  Vector3Event.withTime(int time, this.x, this.y, this.z) : super.withTime(time);
  final double x;
  final double y;
  final double z;
}

class GyroscopeState extends Vector3Event {
  GyroscopeState(super.x, super.y, super.z);
  GyroscopeState.withTime(int time, double x, double y, double z) : super.withTime(time, x, y, z);
  @override
  Map toMap() {
    return {
      'event': 'gyroscope',
      'time': time,
      'x': x,
      'y': y,
      'z': z
    };
  }
}

class GyroscopeCubit extends Cubit<GyroscopeState> {

  StreamSubscription<GyroscopeEvent>? sensorSubscription;
  StreamSubscription? initSubscription;
  BuildContext context;
  var config = Config();

  GyroscopeCubit(this.context) : super(GyroscopeState(0, 0, 0)) {
    motionSensors.gyroscopeUpdateInterval = UPDATE_INTERVAL;
    InitCubit initCubit = context.read<InitCubit>();
    initSubscription = initCubit.stream.listen(onInit);
  }

  Future<void> onInit(InitEvent event) async {
    if (event == InitEvent.INITIALIZED) {
      bool enable = await Config().getEnableGyroscope();
      setEnable(enable);
    }
  }

  Future<void> setEnable(bool enabled) async {
    await config.setEnableGyroscope(enabled);
    if (enabled && sensorSubscription == null) {
      sensorSubscription = motionSensors.gyroscope.listen(onGyroscopeEvent);
    }
    else if (!enabled) {
      sensorSubscription?.cancel();
      sensorSubscription = null;
    }
  }

  @override
  Future<void> close() {
    initSubscription?.cancel();
    sensorSubscription?.cancel();
    return super.close();
  }

  void onGyroscopeEvent(GyroscopeEvent event) {
    emit(GyroscopeState(event.x, event.y, event.z));
  }

}

class UserAccelerometerState extends Vector3Event {
  UserAccelerometerState(super.x, super.y, super.z);
  UserAccelerometerState.withTime(int time, double x, double y, double z) : super.withTime(time, x, y, z);

  @override
  Map toMap() {
    return {
      'event': 'user-accelerometer',
      'time': time,
      'x': x,
      'y': y,
      'z': z
    };
  }
}

class UserAccelerometerCubit extends Cubit<UserAccelerometerState> {

  StreamSubscription<UserAccelerometerEvent>? sensorSubscription;
  StreamSubscription? initSubscription;
  BuildContext context;
  var config = Config();

  UserAccelerometerCubit(this.context) : super(UserAccelerometerState(0, 0, 0)) {
    motionSensors.userAccelerometerUpdateInterval = UPDATE_INTERVAL;
    InitCubit initCubit = context.read<InitCubit>();
    initSubscription = initCubit.stream.listen(onInit);
  }

  Future<void> onInit(InitEvent event) async {
    if (event == InitEvent.INITIALIZED) {
      bool enable = await config.getEnableUserAccelerometer();
      setEnable(enable);
    }
  }

  Future<void> setEnable(bool enabled) async {
    await config.setEnableUserAccelerometer(enabled);
    if (enabled && sensorSubscription == null) {
      sensorSubscription = motionSensors.userAccelerometer.listen(onUserAccelerometerEvent);
    }
    else if (!enabled) {
      sensorSubscription?.cancel();
      sensorSubscription = null;
    }
  }

  @override
  Future<void> close() {
    initSubscription?.cancel();
    sensorSubscription?.cancel();
    return super.close();
  }

  void onUserAccelerometerEvent(UserAccelerometerEvent event) {
    emit(UserAccelerometerState(event.x, event.y, event.z));
  }

}

class AccelerometerState extends Vector3Event {
  AccelerometerState(super.x, super.y, super.z);
  AccelerometerState.withTime(int time, double x, double y, double z) : super.withTime(time, x, y, z);

  @override
  Map toMap() {
    return {
      'event': 'accelerometer',
      'time': time,
      'x': x,
      'y': y,
      'z': z
    };
  }
}

class AccelerometerCubit extends Cubit<AccelerometerState> {

  StreamSubscription<AccelerometerEvent>? sensorSubscription;
  StreamSubscription? initSubscription;
  BuildContext context;
  var config = Config();

  AccelerometerCubit(this.context) : super(AccelerometerState(0, 0, 0)) {
    motionSensors.accelerometerUpdateInterval = UPDATE_INTERVAL;
    InitCubit initCubit = context.read<InitCubit>();
    initSubscription = initCubit.stream.listen(onInit);
  }

  Future<void> onInit(InitEvent event) async {
    if (event == InitEvent.INITIALIZED) {
      bool enable = await config.getEnableAccelerometer();
      setEnable(enable);
    }
  }

  Future<void> setEnable(bool enabled) async {
    await config.setEnableAccelerometer(enabled);
    if (enabled && sensorSubscription == null) {
      sensorSubscription = motionSensors.accelerometer.listen(onAccelerometerEvent);
    }
    else if (!enabled) {
      sensorSubscription?.cancel();
      sensorSubscription = null;
    }
  }

  @override
  Future<void> close() {
    initSubscription?.cancel();
    sensorSubscription?.cancel();
    return super.close();
  }

  void onAccelerometerEvent(AccelerometerEvent event) {
    emit(AccelerometerState(event.x, event.y, event.z));
  }

}



class MagnetometerState extends Vector3Event {
  MagnetometerState(super.x, super.y, super.z);
  MagnetometerState.withTime(int time, double x, double y, double z) : super.withTime(time, x, y, z);

  @override
  Map toMap() {
    return {
      'event': 'magnetometer',
      'time': time,
      'x': x,
      'y': y,
      'z': z
    };
  }
}

class MagnetometerCubit extends Cubit<MagnetometerState> {

  StreamSubscription<MagnetometerEvent>? sensorSubscription;
  StreamSubscription? initSubscription;
  BuildContext context;
  var config = Config();

  MagnetometerCubit(this.context) : super(MagnetometerState(0, 0, 0)) {
    motionSensors.magnetometerUpdateInterval = UPDATE_INTERVAL;
    InitCubit initCubit = context.read<InitCubit>();
    initSubscription = initCubit.stream.listen(onInit);
  }
  
  Future<void> onInit(InitEvent event) async {
    if (event == InitEvent.INITIALIZED) {
      bool enable = await config.getEnableMagnetometer();
      setEnable(enable);
    }
  }

  Future<void> setEnable(bool enabled) async {
    await config.setEnableMagnetometer(enabled);
    if (enabled && sensorSubscription == null) {
      sensorSubscription = motionSensors.magnetometer.listen(onMagnetometerEvent);
    }
    else if (!enabled) {
      sensorSubscription?.cancel();
      sensorSubscription = null;
    }
  }

  void onMagnetometerEvent(MagnetometerEvent event) {
    emit(MagnetometerState(event.x, event.y, event.z));
  }

  @override
  Future<void> close() {
    initSubscription?.cancel();
    sensorSubscription?.cancel();
    return super.close();
  }

}

class OrientationState extends InstantEvent {
  OrientationState(this.pitch, this.yaw, this.roll);
  OrientationState.withTime(int time, this.pitch, this.yaw, this.roll) : super.withTime(time);

  final double pitch;
  final double yaw;
  final double roll;

  @override
  Map toMap() {
    return {
      'event': 'orientation',
      'time': time,
      'pitch': pitch,
      'yaw': yaw,
      'roll': roll
    };
  }
}

class OrientationCubit extends Cubit<OrientationState> {

  StreamSubscription<AbsoluteOrientationEvent>? sensorSubscription;
  StreamSubscription? initSubscription;
  BuildContext context;
  var config = Config();

  OrientationCubit(this.context) : super(OrientationState(0, 0, 0)) {
    motionSensors.absoluteOrientationUpdateInterval = UPDATE_INTERVAL;
    InitCubit initCubit = context.read<InitCubit>();
    initSubscription = initCubit.stream.listen(onInit);
  }

  void onOrientationEvent(AbsoluteOrientationEvent event) {
    emit(OrientationState(event.pitch, event.yaw, event.roll));
  }

  Future<void> onInit(InitEvent event) async {
    if (event == InitEvent.INITIALIZED) {
      bool enable = await config.getEnableOrientation();
      setEnable(enable);
    }
  }
  
  Future<void> setEnable(bool enabled) async {
    await config.setEnableOrientation(enabled);
    if (enabled && sensorSubscription == null) {
      sensorSubscription = motionSensors.absoluteOrientation.listen(onOrientationEvent);
    }
    else if (!enabled) {
      sensorSubscription?.cancel();
      sensorSubscription = null;
    }
  }

  @override
  Future<void> close() {
    initSubscription?.cancel();
    sensorSubscription?.cancel();
    return super.close();
  }

}

class BatteryState extends InstantEvent {
  BatteryState(this.state, this.level);
  BatteryState.withTime(int time, this.state, this.level) : super.withTime(time);

  final String state;
  final int level;

  @override
  Map toMap() {
    return {
      'event': 'battery',
      'time': time,
      'state': state,
      'level': level
    };
  }
}

class BatteryCubit extends Cubit<BatteryState> {

  var battery = Battery();
  Timer? timer;
  StreamSubscription? initSubscription;
  BuildContext context;
  var config = Config();

  BatteryCubit(this.context) : super(BatteryState('unknown', 0)) {
    InitCubit initCubit = context.read<InitCubit>();
    initSubscription = initCubit.stream.listen(onInit);
  }

  Future<void> onBatteryEvent(Timer t) async {
    var batteryState = await battery.batteryState;
    var batteryLevel = await battery.batteryLevel;
    emit(BatteryState(batteryState.name, batteryLevel));
  }

  Future<void> onInit(InitEvent event) async {
    if (event == InitEvent.INITIALIZED) {
      bool enable = await config.getEnableBattery();
      setEnable(enable);
    }
  }

  Future<void> setEnable(bool enabled) async {
    await config.setEnableBattery(enabled);
    if (enabled && timer == null) {
      timer = Timer.periodic(Duration(seconds:10), onBatteryEvent);
      await onBatteryEvent(timer!);
    }
    else if (!enabled) {
      timer?.cancel();
      timer = null;
    }
  }

  @override
  Future<void> close() {
    initSubscription?.cancel();
    timer?.cancel();
    return super.close();
  }

}

class TemperatureState extends InstantEvent {
  TemperatureState(this.temperature);
  TemperatureState.withTime(int time, this.temperature) : super.withTime(time);

  final double temperature;

  @override
  Map toMap() {
    return {
      'event': 'temperature',
      'time': time,
      'temperature': temperature
    };
  }
}

class TemperatureCubit extends Cubit<TemperatureState> {

  Timer? timer;
  StreamSubscription? initSubscription;
  BuildContext context;
  var config = Config();

  TemperatureCubit(this.context) : super(TemperatureState(0)) {
    InitCubit initCubit = context.read<InitCubit>();
    initSubscription = initCubit.stream.listen(onInit);
  }


  Future<void> onTemperatureEvent(Timer t) async {
    double temperature = await Devicetemperature.DeviceTemperature;
    emit(TemperatureState(temperature));
  }

  Future<void> onInit(InitEvent event) async {
    if (event == InitEvent.INITIALIZED) {
      bool enable = await config.getEnableTemperature();
      setEnable(enable);
    }
  }

  Future<void> setEnable(bool enabled) async {
    await config.setEnableTemperature(enabled);
    if (enabled && timer == null) {
      timer = Timer.periodic(Duration(seconds:10), onTemperatureEvent);
      await onTemperatureEvent(timer!);
    }
    else if (!enabled) {
      timer?.cancel();
      timer = null;
    }
  }

  @override
  Future<void> close() {
    initSubscription?.cancel();
    timer?.cancel();
    return super.close();
  }

}
