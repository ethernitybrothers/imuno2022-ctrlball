import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'dart:isolate';
import 'dart:convert';
import 'dart:typed_data';
import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_isolate/flutter_isolate.dart';
import 'package:imuno2022ctrlball/blocs/init.dart';
import 'package:imuno2022ctrlball/blocs/sensors.dart';

import '../config.dart';

const String MSG_CONNECTED = 'CONNECTED';
const String MSG_DISCONNECTED = 'DISCONNECTED';
const String MSG_CONFIG_UPDATE = 'CONFIG_UPDATE';
const String MSG_PAYLOAD = 'PAYLOAD';
const String MSG_ERROR = 'ERROR';

class Payload {
  final payload;

  Payload(this.payload);
}

class PayloadCubit extends Cubit<Payload> {
  BuildContext context;
  PayloadCubit(this.context) : super(Payload({}));

}

class CommState {
  CommState({required this.connected, required this.logs});
  final bool connected;
  final Queue<String> logs;
}

class CommCubit extends Cubit<CommState> {

  BuildContext context;
  late StreamSubscription initSubscription;
  late StreamSubscription gyroscopeSubscription;
  late StreamSubscription acceleratorSubscription;
  late StreamSubscription userAcceleratorSubscription;
  late StreamSubscription magnetometerSubscription;
  late StreamSubscription orientationSubscription;
  late StreamSubscription batterySubscription;
  late StreamSubscription temperatureSubscription;
  late PayloadCubit payloadCubit;

  final Config config = Config();
  late FlutterIsolate isolate;
  ReceivePort receivePort = ReceivePort();
  SendPort? sendPort;

  final Queue<String> logs = Queue();

  CommCubit(this.context) : super(CommState(connected:false, logs: Queue())) {
    // input cubits
    InitCubit initCubit = context.read<InitCubit>();
    GyroscopeCubit gyroscopeCubit = context.read<GyroscopeCubit>();
    AccelerometerCubit accelerometerCubit = context.read<AccelerometerCubit>();
    UserAccelerometerCubit userAccelerometerCubit = context.read<UserAccelerometerCubit>();
    MagnetometerCubit magnetometerCubit = context.read<MagnetometerCubit>();
    OrientationCubit orientationCubit = context.read<OrientationCubit>();
    BatteryCubit batteryCubit = context.read<BatteryCubit>();
    TemperatureCubit temperatureCubit = context.read<TemperatureCubit>();

    initSubscription = initCubit.stream.listen(onInit);
    gyroscopeSubscription = gyroscopeCubit.stream.listen(onGyroscope);
    acceleratorSubscription = accelerometerCubit.stream.listen(onAccelerometer);
    userAcceleratorSubscription = userAccelerometerCubit.stream.listen(onUserAccelerometer);
    magnetometerSubscription = magnetometerCubit.stream.listen(onMagnetometer);
    orientationSubscription = orientationCubit.stream.listen(onOrientation);
    batterySubscription = batteryCubit.stream.listen(onBattery);
    temperatureSubscription = temperatureCubit.stream.listen(onTemperature);

    // output cubit
    payloadCubit = context.read<PayloadCubit>();
  }

  @override
  Future<void> close() async {
    isolate.kill();
    await gyroscopeSubscription.cancel();
    await acceleratorSubscription.cancel();
    await userAcceleratorSubscription.cancel();
    await magnetometerSubscription.cancel();
    await orientationSubscription.cancel();
    await batterySubscription.cancel();
    await temperatureSubscription.cancel();
    return super.close();
  }

  void setServerConnect(bool connect) {
    if (sendPort != null) {
      var msg = { 'type': MSG_CONFIG_UPDATE, 'shouldConnect': connect };
      sendPort!.send(msg);
    }
  }

  void setServerHostname(String hostname) {
    if (sendPort != null) {
      var msg = { 'type': MSG_CONFIG_UPDATE, 'hostname': hostname };
      sendPort!.send(msg);
    }
  }

  void setServerPort(int port) {
    if (sendPort != null) {
      var msg = { 'type': MSG_CONFIG_UPDATE, 'port': port };
      sendPort!.send(msg);
    }
  }

  void setCtrlBallName(String name) {
    if (sendPort != null) {
      var msg = { 'type': MSG_CONFIG_UPDATE, 'ctrlball': name };
      sendPort!.send(msg);
    }
  }

  Future<void> onInit(InitEvent event) async {
    if (event != InitEvent.INITIALIZED) {
      return;
    }

    receivePort = ReceivePort();
    isolate = await FlutterIsolate.spawn(CommThread.entrypoint, receivePort.sendPort);
    receivePort.listen((data) async {
      if (data is SendPort) {
        sendPort = data;
        String hostname = await config.getServerHostname();
        int port = await config.getServerPort();
        bool shouldConnect = await config.getServerConnect();
        String ctrlBallName = await config.getCtrlBallName();
        // initial config
        var msg = { 'type': MSG_CONFIG_UPDATE, 'hostname': hostname, 'port': port, 'shouldConnect': shouldConnect, 'ctrlball': ctrlBallName };
        sendPort!.send(msg);
      }
      else if (data['type'] == MSG_CONNECTED) {
        addLog(MSG_CONNECTED + ": " + data['detail']);
        emit(CommState(connected: true, logs: logs));
      }
      else if (data['type'] == MSG_DISCONNECTED) {
        addLog(MSG_DISCONNECTED);
        emit(CommState(connected: false, logs: logs));
      }
      else if (data['type'] == MSG_ERROR) {
        addLog("error: "+data['error']);
        emit(CommState(connected: false, logs: logs));
      }
      else if (data['type'] == MSG_PAYLOAD) {
        var payload = data['payload'];
        payloadCubit.emit(Payload(payload));
      }
    });
  }

  void addLog(String log) {
    logs.add(log);
    if (logs.length > 10) {
      logs.removeFirst();
    }
  }

  Map msgPayload(Map payload) {
    return { 'type': MSG_PAYLOAD, 'payload': payload };
  }

  void onGyroscope(GyroscopeState state) {
    if (sendPort != null) {
      sendPort!.send(msgPayload(state.toMap()));
    }
  }

  void onAccelerometer(AccelerometerState state) {
    if (sendPort != null) {
      sendPort!.send(msgPayload(state.toMap()));
    }
  }

  void onUserAccelerometer(UserAccelerometerState state) {
    if (sendPort != null) {
      sendPort!.send(msgPayload(state.toMap()));
    }
  }

  void onMagnetometer(MagnetometerState state) {
    if (sendPort != null) {
      sendPort!.send(msgPayload(state.toMap()));
    }
  }

  void onOrientation(OrientationState state) {
    if (sendPort != null) {
      sendPort!.send(msgPayload(state.toMap()));
    }
  }

  void onBattery(BatteryState state) {
    if (sendPort != null) {
      sendPort!.send(msgPayload(state.toMap()));
    }
  }

  void onTemperature(TemperatureState state) {
    if (sendPort != null) {
      sendPort!.send(msgPayload(state.toMap()));
    }
  }

}

enum ConnectionState {
  DISCONNECTED,
  DISCONNECTED_NO_RECONNECT,
  CONNECTING,
  CONNECTED
}

class CommThread {

  final ReceivePort receivePort = ReceivePort();
  final SendPort sendPort;

  Socket? socket;
  ConnectionState connectionState = ConnectionState.DISCONNECTED;

  late String hostname;
  late int port;
  bool shouldConnect = false;
  late String ctrlballName;

  StringBuffer receiveBuffer = StringBuffer();

  CommThread(this.sendPort) {
    sendPort.send(receivePort.sendPort);
    receivePort.listen(onData);
  }


  Future<void> connect() async {
    if (connectionState == ConnectionState.DISCONNECTED) {
      try {
        print('Connecting to $hostname:$port');
        connectionState = ConnectionState.CONNECTING;
        receiveBuffer.clear();
        socket = await Socket.connect(hostname, port, timeout: const Duration(seconds: 5));
        socket?.listen(receiveData);
        connectionState = ConnectionState.CONNECTED;
        var helloPayload = {
          'event': 'hello',
          'ctrlball': ctrlballName,
          'time': InstantEvent.now
        };
        await sendPayload(helloPayload);
        sendPort.send({ 'type': MSG_CONNECTED, 'detail': "$hostname:$port" });
      }
      catch (error) {
        sendPort.send({ 'type': MSG_ERROR, 'error': error.toString() });
        connectionError(error);
      }
    }
  }

  void disconnect() {
    if (connectionState == ConnectionState.CONNECTED) {
      print("Disconnecting");
      socket!.destroy();
      socket = null;
      connectionState = ConnectionState.DISCONNECTED;
      sendPort.send({ 'type': MSG_DISCONNECTED });
    }
  }

  Future<void> maybeConnect() async {
    if (connectionState == ConnectionState.DISCONNECTED) {
      if (shouldConnect) {
        await connect();
      }
    }
  }

  void connectionError(error) {
    connectionState = ConnectionState.DISCONNECTED_NO_RECONNECT;
    print("connection failed: $error");
    // we will only attempt to reconnect after 5 secs not before
    Future.delayed(const Duration(seconds: 5), () => connectionState = ConnectionState.DISCONNECTED);
  }

  void onData(obj) async {
    Map data = obj;
    if (data['type'] == MSG_CONFIG_UPDATE) {
        if (data.containsKey('port')) {
          port = data['port'] as int;
        }
        if (data.containsKey('hostname')) {
          hostname = data['hostname'] as String;
        }
        if (data.containsKey('shouldConnect')) {
          shouldConnect = data['shouldConnect'] as bool;
        }
        if (data.containsKey('ctrlball')) {
          ctrlballName = data['ctrlball'] as String;
        }

        disconnect();
        maybeConnect();
    }
    else if (socket == null) {
      maybeConnect();
    }
    else if (data['type'] == MSG_PAYLOAD) {
      await sendPayload(data['payload'] as Map);
    }
  }

  Future<void> sendPayload(Map payload) async {
    String str = json.encode(payload);
    try {
      socket!.writeln(str);
      await socket!.flush();
    }
    catch (error) {
      sendPort.send({ 'type': MSG_ERROR, 'error': error.toString() });
      print("ERROR: $error");
      disconnect();
    }
  }
  
  void receiveData(Uint8List data) {
    final serverResponse = String.fromCharCodes(data);
    receiveBuffer.write(serverResponse);
    var stringBuffer = receiveBuffer.toString();
    var messages = <String>[];
    while (stringBuffer.contains("\n")) {
      var lines = stringBuffer.split("\n");
      messages.add(lines.first);
      stringBuffer = lines.sublist(1).join("\n");
    }

    messages.forEach(receiveMessage);
    receiveBuffer = StringBuffer(stringBuffer);
  }

  void receiveMessage(String message) {
    print("Recv: $message");
    var payload = json.decode(message);
    sendPort.send({ 'type': MSG_PAYLOAD, 'payload': payload });
  }

  static void entrypoint(SendPort sendPort) {
    CommThread(sendPort);
  }

}
