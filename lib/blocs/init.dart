
import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum InitEvent {
  NEW,
  INITIALIZED
}

class InitCubit extends Cubit<InitEvent> {

  InitCubit() : super(InitEvent.NEW) {
    WidgetsBinding.instance.addPostFrameCallback(onPostFrame);
  }

  void initFinished() {
    emit(InitEvent.INITIALIZED);
  }

  void onPostFrame(Duration timestamp) {
    if (state == InitEvent.NEW) {
      emit(InitEvent.INITIALIZED);
    }
  }

}