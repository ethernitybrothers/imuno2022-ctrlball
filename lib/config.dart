
import 'package:shared_preferences/shared_preferences.dart';

class Config {

  final Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  Future<String> getServerHostname() async {
    var p = await prefs;
    await p.reload();
    return p.getString('server_hostname') ?? '192.168.1.1';
  }

  Future<int> getServerPort() async {
    var p = await prefs;
    await p.reload();
    return p.getInt('server_port') ?? 12223;
  }

  Future<bool> getServerConnect() async {
    var p = await prefs;
    await p.reload();
    return p.getBool('server_connect') ?? false;
  }

  Future<String> getCtrlBallName() async {
    var p = await prefs;
    await p.reload();
    return p.getString('ctrlball_name') ?? 'player1';
  }

  Future<bool> getEnableOrientation() async {
    var p = await prefs;
    await p.reload();
    return p.getBool('sensor_orientation') ?? false;
  }

  Future<bool> getEnableMagnetometer() async {
    var p = await prefs;
    await p.reload();
    return p.getBool('sensor_magnetometer') ?? false;
  }

  Future<bool> getEnableGyroscope() async {
    var p = await prefs;
    await p.reload();
    return p.getBool('sensor_gyroscope') ?? false;
  }

  Future<bool> getEnableUserAccelerometer() async {
    var p = await prefs;
    await p.reload();
    return p.getBool('sensor_user_acceleration') ?? false;
  }

  Future<bool> getEnableAccelerometer() async {
    var p = await prefs;
    await p.reload();
    return p.getBool('sensor_acceleration') ?? false;
  }

  Future<bool> getEnableBattery() async {
    var p = await prefs;
    await p.reload();
    return p.getBool('sensor_battery') ?? false;
  }

  Future<bool> getEnableTemperature() async {
    var p = await prefs;
    await p.reload();
    return p.getBool('sensor_temperature') ?? false;
  }

  Future<bool> setServerHostname(String value) async {
    var p = await prefs;
    return await p.setString('server_hostname', value);
  }

  Future<bool> setServerPort(int value) async {
    var p = await prefs;
    return await p.setInt('server_port', value);
  }

  Future<bool> setServerConnect(bool value) async {
    var p = await prefs;
    return await p.setBool('server_connect', value);
  }

  Future<bool> setCtrlBallName(String value) async {
    var p = await prefs;
    return await p.setString('ctrlball_name', value);
  }

  Future<bool> setEnableOrientation(bool value) async {
    var p = await prefs;
    return await p.setBool('sensor_orientation', value);
  }

  Future<bool> setEnableMagnetometer(bool value) async {
    var p = await prefs;
    return await p.setBool('sensor_magnetometer', value) ;
  }

  Future<bool> setEnableGyroscope(bool value) async {
    var p = await prefs;
    return await p.setBool('sensor_gyroscope', value);
  }

  Future<bool> setEnableUserAccelerometer(bool value) async {
    var p = await prefs;
    return await p.setBool('sensor_user_acceleration', value);
  }

  Future<bool> setEnableAccelerometer(bool value) async {
    var p = await prefs;
    return await p.setBool('sensor_acceleration', value);
  }

  Future<bool> setEnableBattery(bool value) async {
    var p = await prefs;
    return await p.setBool('sensor_battery', value);
  }

  Future<bool> setEnableTemperature(bool value) async {
    var p = await prefs;
    return await p.setBool('sensor_temperature', value);
  }

}
